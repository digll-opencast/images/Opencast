FROM registry.gitlab.com/digll-opencast/images/opencast-docker/worker:unimr

ENV SOLR_HOST_URL="http://localhost:8983/solr" \
    ELASTICSEARCH_SERVER_ADDRESS="127.0.0.1" \
    ELASTICSEARCH_SERVER_PORT="9300"\
    ORG_OPENCASTPROJECT_NODENAME="Worker"

ENV OPENCAST_HOME="/opencast" \
    OPENCAST_DATA="/data" \
    OPENCAST_CONFIG="${OPENCAST_HOME}/etc" \
    OPENCAST_SCRIPTS="${OPENCAST_HOME}/docker/scripts" \
    OPENCAST_SUPPORT="${OPENCAST_HOME}/docker/support"

RUN  /bin/sed 's/main/main non-free/g' -i /etc/apt/sources.list \
  && /bin/echo 'APT::Default-Release stable;' > /etc/apt/apt.conf.d/default-release \
  && /bin/echo 'deb http://deb.debian.org/debian testing main non-free' >> /etc/apt/sources.list \
  && /bin/echo 'deb http://deb.debian.org/debian testing-updates main non-free' >> /etc/apt/sources.list \
  && apt-get update \
  && DEBIAN_FRONTEND=noninteractive apt-get -t testing install -y libgl1-mesa-glx libgl1-mesa-dri vainfo intel-media-va-driver-non-free i965-va-driver-shaders ffmpeg; /bin/true \
  && rm -rf /var/lib/apt/lists/*

RUN  ln -s /usr/bin/ffmpeg /usr/local/sbin/ffmpeg \
  && ln -s /usr/bin/ffprobe /usr/local/sbin/ffprobe

COPY --chown=opencast:opencast assets/etc/ "${OPENCAST_CONFIG}/"
COPY --chown=opencast:opencast assets/scripts/* "${OPENCAST_SCRIPTS}/"
COPY assets/docker-custom-entrypoint.sh /

WORKDIR "${OPENCAST_HOME}"

EXPOSE 8080
VOLUME [ "${OPENCAST_DATA}" ]

HEALTHCHECK --timeout=10s CMD /docker-healthcheck.sh
ENTRYPOINT ["/docker-custom-entrypoint.sh"]
CMD ["app:start"]
