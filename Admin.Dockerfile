FROM registry.gitlab.com/digll-opencast/images/opencast-docker/admin:unimr

ENV SOLR_HOST_URL="http://localhost:8983/solr" \
    ELASTICSEARCH_SERVER_ADDRESS="127.0.0.1" \
    ELASTICSEARCH_SERVER_PORT="9300" \
    ORG_OPENCASTPROJECT_NODENAME="Admin"

ENV OPENCAST_HOME="/opencast" \
    OPENCAST_DATA="/data" \
    OPENCAST_CONFIG="${OPENCAST_HOME}/etc" \
    OPENCAST_SCRIPTS="${OPENCAST_HOME}/docker/scripts" \
    OPENCAST_SUPPORT="${OPENCAST_HOME}/docker/support"

COPY --chown=opencast:opencast assets/etc/* "${OPENCAST_CONFIG}/"
COPY --chown=opencast:opencast assets/scripts/* "${OPENCAST_SCRIPTS}/"
COPY assets/docker-custom-entrypoint.sh /

WORKDIR "${OPENCAST_HOME}"

EXPOSE 8080
VOLUME [ "${OPENCAST_DATA}" ]

HEALTHCHECK --timeout=10s CMD /docker-healthcheck.sh
ENTRYPOINT ["/docker-custom-entrypoint.sh"]
CMD ["app:start"]
