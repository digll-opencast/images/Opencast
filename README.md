## Docker Opencast ##

This repository contains build instructions for a container image running an Opencast-Admin, Opencast-Presentation
and Opencast-Worker instances by extending the [official](https://github.com/opencast/opencast-docker) images with
additional configuration for centralized Apache Solr and Elasticsearch instances.

For additional information about these images see <https://github.com/opencast/opencast-docker>.

### Building the image ###

Building the image can be achieved with the collowing commands:
```bash
git clone https://gitlab.digll-hessen.de/OC-Cluster/images/Opencast
cd solr

# Build using the docker daemon
sudo docker build --tag opencast-admin:8.6 --tag opencast-admin:latest Admin.Dockerfile
sudo docker build --tag opencast-presentation:8.6 --tag opencast-presentation:latest Presentation.Dockerfile
sudo docker build --tag opencast-worker:8.6 --tag opencast-worker:latest Worker.Dockerfile

# Build using the buildah tool
sudo buildah build-using-dockerfile --tag opencast-admin:8.6 --tag opencast-admin:latest -f Admin.Dockerfile
sudo buildah build-using-dockerfile --tag opencast-presentation:8.6 --tag opencast-presentation:latest -f Presentation.Dockerfile
sudo buildah build-using-dockerfile --tag opencast-worker:8.6 --tag opencast-admin:worker -f Worker.Dockerfile
```

### Publishing the image ###

The image should to pushed to your local (*gitlab*) image registry for use in your container
infrastructure. The image contains Gitlab CI-Runner job configurations for building and pushing build-artifacts
to your local container registry, assuming it has support for this. (**WIP**)

Manually pushing image using *buildah* tool:
```bash
sudo buildah push opencast-admin:1.4.1 docker://<registry-url>:1.4.1
sudo buildah push opencast-admin:latest docker://<registry-url>:latest

sudo buildah push opencast-presentation:1.4.1 docker://<registry-url>:1.4.1
sudo buildah push opencast-presentation:latest docker://<registry-url>:latest

sudo buildah push opencast-worker:1.4.1 docker://<registry-url>:1.4.1
sudo buildah push opencast-worker:latest docker://<registry-url>:latest
```
(*Replace `buildah` with `docker` when using the docker deamon*)

### Using the image ###

The image can be pulled and started with the following commands:

```bash
sudo podman pull <registry-url>/opencast-admin:8.6
sudo podman run \
  --name opencast-presentation \
  --env='ORG_OPENCASTPROJECT_SERVER_URL=http://opencast-admin:8080' \
  --env='ORG_OPENCASTPROJECT_DOWNLOAD_URL=http://localhost:8080/static' \
  --env='ORG_OPENCASTPROJECT_SECURITY_ADMIN_USER=admin' \
  --env='ORG_OPENCASTPROJECT_SECURITY_ADMIN_PASS=opencast' \
  --env='ORG_OPENCASTPROJECT_SECURITY_DIGEST_USER=opencast_system_account' \
  --env='ORG_OPENCASTPROJECT_SECURITY_DIGEST_PASS=CHANGE_ME' \
  --env='ORG_OPENCASTPROJECT_DB_VENDOR=MySQL' \
  --env='ORG_OPENCASTPROJECT_DB_JDBC_URL=jdbc:mysql://mariadb/opencast' \
  --env='ORG_OPENCASTPROJECT_DB_JDBC_USER=opencast' \
  --env='ORG_OPENCASTPROJECT_DB_JDBC_PASS=opencast' \
  --env='PROP_ORG_OPENCASTPROJECT_ADMIN_UI_URL=http://localhost:8080' \
  --env='PROP_ORG_OPENCASTPROJECT_ENGAGE_UI_URL=http://localhost:8081' \
  --env='ACTIVEMQ_BROKER_URL=failover://(tcp://activemq:61616)?initialReconnectDelay=2000&maxReconnectDelay=60000' \
  --env='ACTIVEMQ_BROKER_USERNAME=admin' \
  --env='ACTIVEMQ_BROKER_PASSWORD=password' \
  --env='ELASTICSEARCH_SERVER_ADDRESS=elasticsearch' \
  --env='SOLR_HOST_URL=http://solr:8983/solr' \
  --env='ORG_OPENCASTPROJECT_NODENAME=Admin' \
  --volume=oc:/data \
  -p 8080:8080 \
  opencast-admin:8.6

sudo podman pull <registry-url>/opencast-presentation:8.6
sudo podman run \
  --name opencast-presentation \
  --env='ORG_OPENCASTPROJECT_SERVER_URL=http://opencast-presentation:8080' \
  --env='ORG_OPENCASTPROJECT_DOWNLOAD_URL=http://localhost:8080/static' \
  --env='ORG_OPENCASTPROJECT_SECURITY_ADMIN_USER=admin' \
  --env='ORG_OPENCASTPROJECT_SECURITY_ADMIN_PASS=opencast' \
  --env='ORG_OPENCASTPROJECT_SECURITY_DIGEST_USER=opencast_system_account' \
  --env='ORG_OPENCASTPROJECT_SECURITY_DIGEST_PASS=CHANGE_ME' \
  --env='ORG_OPENCASTPROJECT_DB_VENDOR=MySQL' \
  --env='ORG_OPENCASTPROJECT_DB_JDBC_URL=jdbc:mysql://mariadb/opencast' \
  --env='ORG_OPENCASTPROJECT_DB_JDBC_USER=opencast' \
  --env='ORG_OPENCASTPROJECT_DB_JDBC_PASS=opencast' \
  --env='PROP_ORG_OPENCASTPROJECT_ADMIN_UI_URL=http://localhost:8080' \
  --env='PROP_ORG_OPENCASTPROJECT_ENGAGE_UI_URL=http://localhost:8081' \
  --env='ACTIVEMQ_BROKER_URL=failover://(tcp://activemq:61616)?initialReconnectDelay=2000&maxReconnectDelay=60000' \
  --env='ACTIVEMQ_BROKER_USERNAME=admin' \
  --env='ACTIVEMQ_BROKER_PASSWORD=password' \
  --env='ELASTICSEARCH_SERVER_ADDRESS=elasticsearch' \
  --env='SOLR_HOST_URL=http://solr:8983/solr' \
  --env='ORG_OPENCASTPROJECT_NODENAME=Presentation01' \
  --volume=oc:/data \
  -p 8081:8080 \
  opencast-presentation:8.6

sudo podman pull <registry-url>/opencast-worker:8.6
sudo podman run \
  --name opencast-worker \
  --env='ORG_OPENCASTPROJECT_SERVER_URL=http://opencast-worker:8080' \
  --env='ORG_OPENCASTPROJECT_DOWNLOAD_URL=http://localhost:8080/static' \
  --env='ORG_OPENCASTPROJECT_SECURITY_ADMIN_USER=admin' \
  --env='ORG_OPENCASTPROJECT_SECURITY_ADMIN_PASS=opencast' \
  --env='ORG_OPENCASTPROJECT_SECURITY_DIGEST_USER=opencast_system_account' \
  --env='ORG_OPENCASTPROJECT_SECURITY_DIGEST_PASS=CHANGE_ME' \
  --env='ORG_OPENCASTPROJECT_DB_VENDOR=MySQL' \
  --env='ORG_OPENCASTPROJECT_DB_JDBC_URL=jdbc:mysql://mariadb/opencast' \
  --env='ORG_OPENCASTPROJECT_DB_JDBC_USER=opencast' \
  --env='ORG_OPENCASTPROJECT_DB_JDBC_PASS=opencast' \
  --env='PROP_ORG_OPENCASTPROJECT_ADMIN_UI_URL=http://localhost:8080' \
  --env='PROP_ORG_OPENCASTPROJECT_ENGAGE_UI_URL=http://localhost:8081' \
  --env='ACTIVEMQ_BROKER_URL=failover://(tcp://activemq:61616)?initialReconnectDelay=2000&maxReconnectDelay=60000' \
  --env='ACTIVEMQ_BROKER_USERNAME=admin' \
  --env='ACTIVEMQ_BROKER_PASSWORD=password' \
  --env='ELASTICSEARCH_SERVER_ADDRESS=elasticsearch' \
  --env='SOLR_HOST_URL=http://solr:8983/solr' \
  --env='ORG_OPENCASTPROJECT_NODENAME=Worker01' \
  --volume=oc:/data \
  --device=/dev/dri:/dev/dri:rw \
  opencast-worker:8.6
```
(*Replace `buildah` with `docker` when using the docker deamon*)

Note that this requires working **MySQL/MariaDB** database under <mariadb:3306>,
working **Apache ActiveMQ** under <activemq:61616>, working multi-core **Apache Solr**
under <solr:8983> as well as working **Elasticsearch** under <elasticsearch:8983>.

### Configuration ###

This implementation extends the official Opencast container images under <https://github.com/opencast/opencast-docker>
with the following additional environment variables:

-   `SOLR_HOST_URL` (**Required**) Sets the URL under which the multi-core (`/search`, `/series`, `/workflow`) Apache Solr instances (*Version 1.4.x*) is reachable  
    *Note:* Technically the Admin-Module only requires the `/workflow` core, while the Presentation-Module requires only the `/search` and `/series` cores.
    The Worker-Module does not require any Apache Solr instance/core.
-   `ELASTICSEARCH_SERVER_ADDRESS` (**Required**) Sets the hostname/ip-address under which the Elasticsarch instance (*Version 5.6.x*) is reachable.
-   `ELASTICSEARCH_SERVER_PORT` (**Optional**) Sets the port under which the Elasticsarch instance (*Version 5.6.x*) is reachable for the given hostname/ip-address.
-   `ORG_OPENCASTPROJECT_NODENAME` (**Optional**) Sets the NodeName that is displayed in the admin-interface server/services overview

For addition configuration options see the [documentation](https://github.com/opencast/opencast-docker) of the official images.

### Persistence ###

As with the official docker image(s), the default location for **shared** data between all Opencast-Modules
can be found under `/data` and should be managed via a (*shared*) volume to ensure persistent data.

### Updating assets ###

The image uses the `custom.properties` copied from the official docker image repository extended
with templated environment-variable values for a remote Apache Solr instance whose values are now uncommented/enabled:
-   `org.opencastproject.search.solr.url`
-   `org.opencastproject.series.solr.url`
-   `org.opencastproject.workflow.solr.url`

As well as templated environment-variable values for a remote Elasticsearch  instance whose values are now uncommented/enabled:
-   `org.opencastproject.elasticsearch.server.address`
-   `org.opencastproject.elasticsearch.server.port`

In addition to the templated environment-variable values for settings the name of the opencast-node via:
-   `org.opencastproject.server.nodename`
