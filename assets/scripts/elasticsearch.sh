#!/bin/sh

set -e

opencast_elasticsearch_check() {
  echo "Run opencast_elasticsearch_check"

  opencast_helper_checkforvariables \
    "ELASTICSEARCH_SERVER_ADDRESS" \
    "ELASTICSEARCH_SERVER_PORT"
}

opencast_elasticsearch_configure() {
  echo "Run opencast_elasticsearch_configure"

  opencast_helper_replaceinfile "etc/custom.properties" \
    "ELASTICSEARCH_SERVER_ADDRESS" \
    "ELASTICSEARCH_SERVER_PORT"
}
