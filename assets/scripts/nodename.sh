#!/bin/sh

set -e

opencast_nodename_check() {
  echo "Run opencast_nodename_check"

  opencast_helper_checkforvariables \
    "ORG_OPENCASTPROJECT_NODENAME"
}

opencast_nodename_configure() {
  echo "Run opencast_nodename_configure"

  opencast_helper_replaceinfile "etc/custom.properties" \
    "ORG_OPENCASTPROJECT_NODENAME"
}
