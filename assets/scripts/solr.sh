#!/bin/sh

set -e

opencast_solr_check() {
  echo "Run opencast_solr_check"

  opencast_helper_checkforvariables \
    "SOLR_HOST_URL"
}

opencast_solr_configure() {
  echo "Run opencast_solr_configure"

  opencast_helper_replaceinfile "etc/custom.properties" \
    "SOLR_HOST_URL"
}
