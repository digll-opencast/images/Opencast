#!/bin/sh

set -e

opencast_hwaccel_check() {
  echo "Run opencast_hwaccel_check"
}

opencast_hwaccel_configure() {
  echo "Run opencast_hwaccel_configure"

  [ ! -d /dev/dri ] && {
    echo "/dev/dri missing - bailing out"
    return 0
  }
  [ -c /dev/dri/renderD128 ] && {
    chown opencast /dev/dri/renderD128
  }
}
