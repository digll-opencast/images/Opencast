#!/bin/sh

set -e

# shellcheck source=./scripts/helper.sh
. "${OPENCAST_SCRIPTS}/helper.sh"
# shellcheck source=./scripts/elasticsearch.sh
. "${OPENCAST_SCRIPTS}/elasticsearch.sh"
# shellcheck source=./scripts/solr.sh
. "${OPENCAST_SCRIPTS}/nodename.sh"
# shellcheck source=./scripts/solr.sh
. "${OPENCAST_SCRIPTS}/solr.sh"
# shellcheck source=./scripts/hwaccel.sh
. "${OPENCAST_SCRIPTS}/hwaccel.sh"

opencast_custom_check() {
  echo "Run opencast_custom_check"

  opencast_elasticsearch_check
  opencast_nodename_check
  opencast_solr_check
  opencast_hwaccel_check
}

opencast_custom_configure() {
  echo "Run opencast_custom_configure"

  opencast_elasticsearch_configure
  opencast_nodename_configure
  opencast_solr_configure
  opencast_hwaccel_configure
}

opencast_custom_init() {
  echo "Run opencast_custom_init"

  opencast_custom_check
  opencast_custom_configure
}

case ${1} in
  app:init)
    opencast_custom_init
    ;;
  app:start)
    opencast_custom_init
    ;;
esac

/docker-entrypoint.sh "$@"
